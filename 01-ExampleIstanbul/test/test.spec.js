var testCase = require('mocha').describe;
var pre = require('mocha').before;
var assertions = require('mocha').it;
var assert = require('chai').assert;
let AdministradorDeArreglos = require('../index');

testCase('AdministradorDeArreglos', function () {
  pre(function () {
  });

  testCase('Marca BMW en Arrelgo de marcas ["Audi", "Volvo", "BMW"]', function () {
    assertions('should return true when present', function () {
      let administradorDeArreglos = new AdministradorDeArreglos();
      const arregleDeMarcas = ["Audi", "Volvo", "BMW"];
      assert.equal(administradorDeArreglos.ExisteEnArreglo("BMW", arregleDeMarcas), true);
    });
  });

});