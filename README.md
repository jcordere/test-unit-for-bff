# test-unit-for-bff

Examples for test unit in BFF software projects, on a NodeJS server with ExpressJS framework for the implementation of a Generic BFF and on the Test Unit side, mocha, chai, sinon and proxyquire, after which use istanbul to get coverage reports.

--------------------------------------------------------------------

# 00-Example

Example of one test unit in pure javascript code.

# 02-ExampleIstanbulMocha

Example of Test Unit thats cover 100% the code, but the code not satisface the necessity of business. For this reason the important is make test for detect bugs not for get the 100% of the coverage.

# 03-ExampleIstanbulMocha

Example of Test Unit for detect bugs and satisface the necessity of business.

# 04-ExampleIstanbulMochaChaiSinon

Example of Test Unit for BFF generic project software, with the necessary elements to make more Test Unit and detect bugs in code.

# Exercise

Copy the 04 project and make one BFF thats return the top artist with the top 10 track, for that's use:
- Top Artist: http://www.mocky.io/v2/5b8d6e283300005400c159ae
- Top Track: http://www.mocky.io/v2/5b8d6ec93300008600c159af

# 05-ExerciseBase

Example of file structure to use in Exercise.
