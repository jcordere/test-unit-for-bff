//Logica de negocio para obtener el top 1 de artistas
var clientTopArtist = require('../client/clientTopArtist');

async function topArtist(req,res) {
    try{
        //Se obtiene data desde cliente que consume servicio
        var callclientTopArtist = await clientTopArtist(req,res);
        
        //Se le da formato de salida
        callclientTopArtist.data = callclientTopArtist.data.artists.artist[0];

        return callclientTopArtist;
    }catch(e){
        console.error(e);
        res.statusCode = 500;
        res.json({ message: 'Could not retrieve data with internal error' });
    }  
}

module.exports = topArtist;