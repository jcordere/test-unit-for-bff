var express = require('express');
var routes = express.Router();

var topArtist = require('./controller/top_artist');
var topTrackForArtistNumberOne = require('./controller/top_track_for_artist_number_one');
var perfilArtistNumerOne = require('./controller/perfil_artist_numer_one');

routes.get('/top_artist', async function(req, res) {
    try{
        var response = await topArtist(req,res);
        res.statusCode = 200;
        res.send(response.data);
    }catch(e){
        console.error(e);
    }
});

routes.get('/top_track_for_artist_number_one', async function(req, res) {
    try{
        var response = await topTrackForArtistNumberOne(req,res);
        res.statusCode = 200;
        res.send(response.data);
    }catch(e){
        console.error(e);
    }
});

routes.get('/perfil_artist_numer_one', async function(req, res) {
    try{
        var response = await perfilArtistNumerOne(req,res);
        res.statusCode = 200;
        res.send(response.data);
    }catch(e){
        console.error(e);
    }
});

module.exports = routes;