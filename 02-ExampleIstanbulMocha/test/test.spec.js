var testCase = require('mocha').describe;
var pre = require('mocha').before;
var assertions = require('mocha').it;
var assert = require('chai').assert;
let AdministradorDeReales = require('../index');

testCase('AdministradorDeReales', function () {
  pre(function () {
  });

  testCase('Numeros en Reales son pares', function () {
    let administradorDeReales;

    pre(function () {
      administradorDeReales = new AdministradorDeReales();
    });

    assertions('should return true when Real is 2', function () {
      assert.equal(administradorDeReales.esNumeroPar(2), true);
    });

    assertions('should return true when Real is 8', function () {
      assert.equal(administradorDeReales.esNumeroPar(8), true);
    });

    assertions('should return false when Real is 1', function () {
      assert.equal(administradorDeReales.esNumeroPar(1), false);
    });
  });

});