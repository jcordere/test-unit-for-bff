'use strict';

class AdministradorDeReales
{
    /**
     * Metodo para validar si un numero real es par
     * @param pNumero
     */
    esNumeroPar(pNumero )
    {
        let validacion = false;
        if(pNumero == 2 || pNumero == 8){
            validacion = true;
        }
        return validacion;
    }

}

module.exports = AdministradorDeReales