const
    expect = require('chai').expect,
    proxyquire = require('proxyquire'),
    Response = require('mock-express-response');

let req, res;

describe('Ecenario Uno', async () => {
    beforeEach(async () => {
        req = {
            query: {
                module: 'fe-empresas'
            }
        };
        res = new Response();

    });

    it('Resultado correcto cuando response con statusCode 200', async () => {
        await getService("status200").getConfigProps(req, res);
        expect(res.statusCode).to.be.equal(200);
    });

    let getService = (opciones) => {
        return proxyquire('../server/api/config-props/index', {
            'request': {
                get(url, callback) {
                    process.nextTick(function () {
                        if (opciones == "status200") {
                            callback(null, {
                                "statusCode": 200,
                                "headers": {
                                    "content-type": "application/json"
                                }
                            }, JSON.stringify({
                                "name": "fe-empresas",
                                "profiles": ["qa"],
                                "label": "normalmente null",
                                "propertySources": [{
                                    "name": "specific properties path. Ej: /path/to/fe-empresas-qa.yml",
                                    "source": {
                                        "key0": "value0",
                                        "key1": "value1"
                                    }
                                },
                                {
                                    "name": "common properties path. Ej: /path/to/application-qa.yml",
                                    "source": {
                                        "key0": "value0",
                                        "key1": "value1"
                                    }
                                }
                                ],
                                "version": "6a5513ba433009c8b22fbfb1a8543d3d71a9d81f",
                                "state": "normalmente null"

                            })
                            )
                        }
                    })
                }
            },
            'config': {
                config_server: {
                    address: 'http://localhost:80',
                    profile: 'local'
                }

            }
        });
    };
});

describe('Ecenario Dos', async () => {
    beforeEach(async () => {
        req = {
            query: {
                module: 'fe-empresas'
            }
        };
        res = new Response();

    });

    it('Resultado correcto cuando no tiene response', async () => {
        await getService("status200").getConfigProps(req, res);
        expect(res.statusCode).to.be.equal(200);
    });

    let getService = (opciones) => {
        return proxyquire('../server/api/config-props/index', {
            'request': {
                get(url, callback) {
                    process.nextTick(function () {
                        if (opciones == "status200") {
                            callback(null, false, null
                            )
                        }
                    }
                    )
                }
            },
            'config': {
                config_server: {
                    address: 'http://localhost:80',
                    profile: 'local'
                }

            },
            stringToObj(path, value, obj){
                throw new Error('Error');
            }
        });
    };
});

describe('Ecenario Tres', async () => {
    beforeEach(async () => {
        req = {
            query: {
                
            }
        };
        res = new Response();

    });

    it('Resultado correcto cuando response con statusCode 400', async () => {
        await getService("status400").getConfigProps(req, res);
        expect(res.statusCode).to.be.equal(400);
    });


    let getService = (opciones) => {
        return proxyquire('../server/api/config-props/index', {
            'request': {
                get(url, callback) {
                    process.nextTick(function () {
                        if (opciones == "status400") {
                            callback(null, {
                                "statusCode": 200,
                                "headers": {
                                    "content-type": "application/json"
                                }
                            }, JSON.stringify({ "error": 'Debe ingresar todos los datos' })
                            )
                        }
                    })
                }
            },
            'config': {
                config_server: {
                    address: 'http://localhost:80',
                    profile: 'local'
                }

            }
        });
    };
});