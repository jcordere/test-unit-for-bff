const fs = require('fs');
const s = require('shelljs');
s.rm('-rf', 'build');
s.mkdir('build');
s.cp('.env', 'build/.env');
s.mkdir('-p', 'build/server/config');
if (process.env.NODE_ENV !== 'production') {
    s.cp('./server/config/development.js', 'build/server/config/development.js');
} else {
    s.cp('./server/config/production.js', 'build/server/config/production.js');
}
s.mkdir('-p', `build/public${process.env.BASE_BFF_PATH}`);
s.cp('-R', 'public/*', `build/public${process.env.BASE_BFF_PATH}`);
s.mkdir('-p', 'build/server/common/swagger');
/** Swagger configuration */
// s.cp('server/common/swagger/Api.json', 'build/server/common/swagger/Api.json');

/** crearemos los archivos de configuracion Api.json e index.html */
/** Api.json */
var apiJson = require('./server/common/swagger/Api');
fs.writeFile("./build/server/common/swagger/Api.json", JSON.stringify(apiJson), function(err) {
    if(err) {  return console.log(err);  }
    console.log("Api.json creado con exito!");
});
/** index.html */
var html = require('./public/api-explorer/index');
fs.writeFile(`./build/public${process.env.BASE_BFF_PATH}/api-explorer/index.html`, html, function(err) {
    if(err) {  return console.log(err);  }
    s.rm('-rf', `./build/public${process.env.BASE_BFF_PATH}/api-explorer/index.js`);
    console.log("index.html creado con exito!");
});