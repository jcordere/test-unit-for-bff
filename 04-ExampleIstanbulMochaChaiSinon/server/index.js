require('./common/env');
const Server = require('./common/server');
const routes = require('./routes');

module.exports = new Server()
  .router(routes)
  .listen(process.env.PORT);
