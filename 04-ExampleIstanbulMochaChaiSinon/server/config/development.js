'use strict';
/**
 * Configuracion de endpoints
 */

module.exports = {
  config_server: {
    address: 'http://localhost:8888',
    appname: 'fe-empresas-admuser',
    profile: 'integracion'
  }
};
