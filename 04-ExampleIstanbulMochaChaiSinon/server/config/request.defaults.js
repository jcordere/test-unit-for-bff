import rp from 'request-promise';

function createHeaders(token){
  return {
    'Authorization': `Bearer ${token}`,
  }
}

const requestDefaults = {
  timeout: 30000,
  json: true
}

var rpCustom = rp.defaults(requestDefaults)

export {
  rpCustom,
  createHeaders
}