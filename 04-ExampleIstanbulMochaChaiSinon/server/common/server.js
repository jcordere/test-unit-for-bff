'use strict';

const Express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const http = require('http');
const os = require('os');
const ifaces = os.networkInterfaces();
const cookieParser = require('cookie-parser');
const swaggerify = require('./swagger');
// const cors = require('cors');
const helmet = require('helmet');
const app = new Express();

module.exports = class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    app.use(helmet());
    app.set('appPath', `${root}client`);
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(Express.static(`${root}/public`));
    app.use(helmet());
    app.use(helmet.contentSecurityPolicy({
      directives: {
        defaultSrc: ["'self'"],
        styleSrc: ["'self'"],
      },
    }));
    // setea por defecto el content-type como application/json.
    // Puede pisarse si se necesita.
    app.use(function(req, res, next) {
      res.header('Content-Type', 'application/json');
      next();
    });
  }


  router(routes) {
    swaggerify(app, routes);
    return this;
  }


  listen(port = process.env.PORT) {
    const welcome = p => () => {
      console.info(`up and running in ${process.env.NODE_ENV
         || 'development'} @: ${os.hostname()} on port: ${p}`);
    };
    // app.use(errorHandler());
    http.createServer(app).listen(port, welcome(port));
    return app;
  }
};
