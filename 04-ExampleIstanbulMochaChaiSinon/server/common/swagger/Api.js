'use strict';
module.exports = {
	"swagger": "2.0",
	"info": {
		"version": "1.0.0",
		"title": "bff-generico",
		"description": "BFF generico"
	},
	"basePath": process.env.BASE_BFF_PATH,
	"tags": [{
		"name": "Examples",
		"description": "Endpoints TEF"
	},
	{
		"name": "Specification",
		"description": "The swagger API specification"
	}
	],
	"consumes": [
		"text/plain"
	],
	"produces": [
		"application/json"
	],
	"paths": {
		"/config/props": {
			"get": {
				"tags": [
					"props"
				],
				"parameters": [
					{
						"name": "module",
						"in": "query",
						"type": "string",
						"description": "Nombre del modulo",
						"schema": {
							"type": "string",
							"example": "Modulo1"
						},
						"required": false
					}
				],
				"description": "Propiedades de config server para front",
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"type": "object"
						}
					},
					"404": {
						"description": "Not Found"
					},
					"500": {
						"description": "Error al invocar al servicio"
					}
				}
			}
		}
	}
}