/** environment */
const config = require('config');
 
/** endpoint */
const ENDPOINT = `${config.config_server.address}/${config.config_server.appname}/${config.config_server.profile}`;
 
/** export */
module.exports = ENDPOINT;