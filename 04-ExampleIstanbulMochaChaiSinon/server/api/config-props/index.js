var request = require('request');
/** environment */
const config = require('config');


exports.getConfigProps = async (req, res) => {
    if (
        req.query.module === null ||
        req.query.module === undefined ||
        req.query.module === ''
    ) {
        return res.status(400).send({ error: 'Debe ingresar todos los datos' });
    }

    const requestModule = req.query.module;
        /** endpoint */
        const ENDPOINT = `${config.config_server.address}/` + requestModule + `/${config.config_server.profile}`;
        const url = ENDPOINT;
        let propsMap = [];
        var obj = {};

        request.get({
            headers: { 'content-type': 'application/json' },
            url: url
        }, function (error, response, body) {
            if (response) {
                if (response.statusCode === 200) {
                    try {
                        body = JSON.parse(body); // transforma la respuesta en un JSON
                        var propertySources = body["propertySources"];
                        var propsMap = {};
                        var fileName = body["name"] + ((body["profiles"].length == 1) ? "-" + body["profiles"][0] : "");
                        for (var i = 0; i < propertySources.length; i++) {
                            var propertySource = propertySources[i];
                            var propName = propertySource["name"];
                            var idx = propName.indexOf(fileName);
                            //Solo considera las propiedades especificas, no las genericas
                            if (idx != -1) {
                                var sources = propertySource["source"];
                                for (var key in sources) {
                                    var value = sources[key];
                                    propsMap[key] = value;
                                    stringToObj(key, value, obj);
                                }
                            }
                        }
                        res.statusCode = response.statusCode;
                        res.send(obj);
                    } catch (error) {
                        res.status(500).send('Error interno transformando el response');
                    }
                } else {
                    res.status(response.statusCode).send('Error interno al obtener configuracion');
                }
            } else {
                res.status(500).send('Error interno al obtener configuracion');
            }
        });
    

}

function stringToObj(path, value, obj) {
    var parts = path.split("."), part;
    var last = parts.pop();
    while (part = parts.shift()) {
        if (typeof obj[part] != "object") obj[part] = {};
        obj = obj[part]; // update "pointer"
    }
    obj[last] = value;
}

