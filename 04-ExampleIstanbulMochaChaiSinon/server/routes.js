'use strict';

const configProps = require('./api/config-props');

module.exports = function routes(app) {
  app.use(`${process.env.BASE_BFF_PATH}/config/props`, configProps.getConfigProps);
};