'use strict';

class AdministradorDeArreglos
{
    /**
     * Metodo para validar si un elemento existe en un arreglo
     * @param pPalabra
     * @param pArreglo 
     */
    ExisteEnArreglo(pPalabra, pArreglo )
    {
        let validacion = false;
        pArreglo.forEach(element => {
            if(pPalabra == element){
                validacion = true;
            }
        });
        
        return validacion;
    }

}

module.exports = AdministradorDeArreglos