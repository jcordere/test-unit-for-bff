var testCase = require('mocha').describe;
var pre = require('mocha').before;
var assertions = require('mocha').it;
var assert = require('chai').assert;
let AdministradorDeReales = require('../index');

testCase('AdministradorDeReales', function () {
  pre(function () {
  });

  testCase('Numeros en Reales son pares', function () {
    let administradorDeReales;

    pre(function () {
      administradorDeReales = new AdministradorDeReales();
    });

    assertions('should return true when Number is 190124873842', function () {
      assert.equal(administradorDeReales.esNumeroPar(190124873842), true);
    });

    assertions('should return true when Number is -190124873842', function () {
      assert.equal(administradorDeReales.esNumeroPar(-190124873842), true);
    });

    assertions('should return false when Number is 398248761', function () {
      assert.equal(administradorDeReales.esNumeroPar(398248761), false);
    });

    assertions('should return false when String is Pares', function () {
      assert.equal(administradorDeReales.esNumeroPar('Pares'), false);
    });

  });

  testCase('Numeros en Reales son Enteros', function () {
    let administradorDeReales;

    pre(function () {
      administradorDeReales = new AdministradorDeReales();
    });

    assertions('should return true when Number is -5', function () {
      assert.equal(administradorDeReales.esEntero(-5), true);
    });

    assertions('should return true when Number is 0', function () {
      assert.equal(administradorDeReales.esEntero(0), true);
    });

    assertions('should return true when Number is 26798', function () {
      assert.equal(administradorDeReales.esEntero(26798), true);
    });

    assertions('should return false when Number is 2.34', function () {
      assert.equal(administradorDeReales.esEntero(2.34), false);
    });

    assertions('should return false when String is a', function () {
      assert.equal(administradorDeReales.esEntero('a'), false);
    });
  });

});