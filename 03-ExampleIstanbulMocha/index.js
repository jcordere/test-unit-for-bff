'use strict';

class AdministradorDeReales
{
    /**
     * Metodo para validar si un numero es par de acuerdo a documentacion https://es.wikipedia.org/wiki/N%C3%BAmeros_pares_e_impares
     * @param pNumero
     */
    esNumeroPar(pNumero )
    {
        let validacion = false;
        if( this.esEntero(pNumero) && pNumero % 2 == 0){
            validacion = true;
        }
        return validacion;
    }

    /**
     * Metodo para validar si un numero es Entero
     * @param {*} pNumero 
     */
    esEntero(pNumero){
        const regexp = /^[0-9\-]*$/;
	    return regexp.test(pNumero);
    }

}

module.exports = AdministradorDeReales